PACKAGE=pango-fonts
VERSION=0.3

SUBDIRS=indic

all:
	for i in $(SUBDIRS) ; do 		\
	        echo "Making all in $$i"; 	\
		(cd $$i && $(MAKE) all);	\
	done

dist:
	files=`find . -type f -print | grep -v '^#\|CVS\|.pcf|fonts.dir$$'`;	\
	distdir="$(PACKAGE)-$(VERSION)";				\
	for i in $$files ; do						\
		dir=`echo $$i | sed s/[^\/]*$$//`;			\
		test -d $$distdir/$$dir || mkdir -p $$distdir/$$dir;	\
		cp $$i $$distdir/$$dir;					\
	done;								\
	GZIP="--best" tar chozf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION);	\
	rm -rf $(PACKAGE)-$(VERSION)
